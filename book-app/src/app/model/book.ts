export class Book {
    constructor(
        public bookId: number,
        public isbn: string,
        public bookTitle: string,
        public bookAuthor: string,
        public bookDescription: string) { }
}
