import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Book } from '../model/book';

@Injectable({
  providedIn: 'root'
})

export class BooksService {

  private baseUrl = 'http://localhost:8080/api/book/';
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private options = { headers: this.headers };

  constructor(private _http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this._http.get<Book[]>(this.baseUrl).pipe(
      catchError(this.errorHandler('getBooks', [])));
  }

  getBookById(id: number): Observable<Book> {

    return this._http.get<Book>(this.baseUrl + id,
      this.options).pipe(
        catchError(this.errorHandler('getBookById',
          new Book(0, '', '', '', ''))));
  }

  addBooks(book): Observable<Book> {
    return this._http.post<Book>(this.baseUrl ,
      JSON.stringify(book), this.options).pipe(
        catchError(this.errorHandler('addBooks',
          new Book(0, '', '', '', ''))));
  }

  updateBook(book: Book): Observable<Book> {
    return this._http.put<Book>(this.baseUrl + book.bookId,
      JSON.stringify(book), this.options).pipe(
        catchError(this.errorHandler('updateBook',
          new Book(0, '', '', '', ''))));
  }

  deleteBook(id: number) {
    return this._http.delete<Book>(this.baseUrl + '/' + id,
      this.options).pipe(
        catchError(this.errorHandler('deleteBook')));
  }

  errorHandler<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
