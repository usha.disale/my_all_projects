import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Book } from '../model/book';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html'
})
export class BookAddComponent {

  public book: Book = new Book(0, '', '', '', '');

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksServices: BooksService) { }

  addBook() {
    this.booksServices.addBooks(this.book).subscribe(() => {
      this.router.navigate(['/books']);
    });
  }

  gotoBooks() {
    this.router.navigate(['/books']);
  }

}
