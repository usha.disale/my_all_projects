import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent } from './book-list/book-list.component';
import { BookAddComponent } from './book-add/book-add.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookUpdateComponent } from './book-update/book-update.component';

const routes: Routes = [
  {path: '', redirectTo: 'books', pathMatch: 'full'},
  {path: 'books', component: BookListComponent },
  {path: 'bookDetails/:bookId', component: BookDetailsComponent},
  {path: 'bookUpdate/:bookId', component: BookUpdateComponent},
  {path: 'addBooks', component: BookAddComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const bookComponent = [
  BookListComponent,
  BookAddComponent,
  BookDetailsComponent,
  BookUpdateComponent
];
