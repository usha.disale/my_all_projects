import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Book } from '../model/book';
import { BooksService } from '../services/books.service';


@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html'
})
export class BookUpdateComponent implements OnInit {

  public book: Book;
  public id: number;

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksService: BooksService) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((params: ParamMap) => {
      this.id = parseInt(params.get('bookId'), 10);
      this.booksService.getBookById(this.id).subscribe(data => this.book = data);
    });
  }

  addBook() {
    this.booksService.updateBook(this.book).subscribe(() =>
    this.router.navigate(['/books']));
  }

  // cancel() {
  //   const books = this.booksService.getBooks();
  //   books[this.id] = this.book;
  //   this.router.navigate(['/books']);
  // }

}
